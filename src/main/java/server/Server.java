package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Server implements Runnable {

    private static final String DELIMITER = "#";

    private static final File QUOTES_FILE = new File("src/main/resources/quotes.txt");

    private static final int PORT = 4000;

    private final ServerSocket serverSocket = new ServerSocket(PORT);

    private final List<String> quotes = new ArrayList<>();

    private Server() throws IOException {
        loadQuotes();
    }

    private void loadQuotes() {
        try (final BufferedReader reader = new BufferedReader(new FileReader(QUOTES_FILE))){
            String line = reader.readLine();

            while (line != null) {
                quotes.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String[] getRandomQuote() {
        final int randomIndex = ThreadLocalRandom.current().nextInt(0, quotes.size());

        return quotes.get(randomIndex).split(DELIMITER);
    }

    private boolean running = true;

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        try {
            while (running) {
                final Socket socket = serverSocket.accept();

                final BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                final PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

                new Thread(new ClientHandler(this, socket, input, output)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            new Thread(new Server()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
