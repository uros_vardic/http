package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

class ClientHandler implements Runnable {

    private final Server server;

    private final Socket client;

    private final BufferedReader input;

    private final PrintWriter output;

    ClientHandler(Server server, Socket client, BufferedReader input, PrintWriter output) {
        this.server = server;
        this.client = client;
        this.input  = input;
        this.output = output;
    }

    @Override
    public void run() {
        try {
            printRequest();
            sendResponse();
            closeCommunication();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printRequest() throws IOException {
        String request = input.readLine();
        System.out.println("Client HTTP Request:");
        do {
            System.out.println(request);
            request = input.readLine();
        } while (!request.trim().equals(""));

        System.out.println();
    }

    private void sendResponse() {
        final String[] randomQuote = server.getRandomQuote();

        output.println("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n" +
                "<html><head><title>Quote</title></head>\n" +
                "<body><h1>" + randomQuote[0] + "</h1>\n" +
                "<h3>" + randomQuote[1] + "</h3></body>");
    }

    private void closeCommunication() throws IOException {
        input.close();
        output.close();
        client.close();
    }

    @Override
    public String toString() {
        return String.format("ClientHandler{client=%s, input=%s, output=%s}", client, input, output);
    }
}
